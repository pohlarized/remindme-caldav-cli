# Remindme caldav CLI

This is a tiny program that allows you to write natural language reminders in your terminal and add corresponding TODOs to your CalDAV calendar.
It also allows you to fetch a list of the currently existing TODOs.

## Configuration

Copy the `config.example.toml` file to `config.toml` and fill it with values.
Especially the values for keys starting with `caldav_` have to be filled properly, all the other values can stay at their default values.

## Usage

```
usage: remindme [-h] remindme_string [remindme_string ...]

CalDAV reinder CLI.

positional arguments:
  remindme_string  A string in the format `<when> <separator> <what>` where <when> is a description *when* you want to be reminded, and <what> is *what* you want to be reminded of. <separator> can be one of the following: [' that ', ' to ', ' about ', ' abuot ', ' about', ' to']. If the string is just `reminders`, shows all current reminders.

options:
  -h, --help       show this help message and exit
```
