#!/usr/bin/env python3

import argparse
import datetime
import textwrap
import tomllib
from pathlib import Path
from typing import NamedTuple
from uuid import uuid4

import dateparser
from caldav.davclient import DAVClient
from caldav.objects import Todo as CaldavTodo
from icalendar import Alarm
from icalendar import Todo as IcalTodo
from pydantic import BaseModel


class ShortTodo(NamedTuple):
    description: str
    timestamp: datetime.datetime


class Config(BaseModel):
    separators: list[str]
    caldav_username: str
    caldav_password: str
    caldav_calendar_link: str
    caldav_base_url: str
    timezone: str


def separate_message(message: str, separators: list[str]) -> list[str]:
    chosen_sep = separators[0]
    first = len(message)
    found = False
    for sep in separators:
        if sep not in message:
            continue
        found = True
        i = message.index(sep)
        if i < first:
            first = i
            chosen_sep = sep
    if not found:
        raise ValueError(
            "You have to separate the reminder time and content by one of your separators."
        )
    return [x.strip() for x in message.split(chosen_sep, 1)]


def parse_reminder(in_str: str, separators: list[str]) -> tuple[str, str]:
    if in_str == "":
        raise ValueError("You have to provide an input string.")
    time_str, reminder_content = separate_message(in_str, separators)
    if len(reminder_content) > 1200:
        raise ValueError("Your message text may not be longer than 1200 characters.")
    return time_str, reminder_content


def add_caldav_todo(date: datetime.datetime, content: str, config: Config):
    with DAVClient(
        url=config.caldav_base_url,
        username=config.caldav_username,
        password=config.caldav_password,
    ) as client:
        calendar = client.calendar(url=config.caldav_calendar_link)
        todo = IcalTodo()
        todo.add("SUMMARY", content)
        todo.add("CREATED", datetime.datetime.now())
        todo.add("LAST-MODIFIED", datetime.datetime.now())
        todo.add("PERCENT-COMPLETE", 0)
        todo.add("UID", uuid4())
        todo.add("dtstart", date)
        alarm = Alarm()
        alarm.add("ACTION", "DISPLAY")
        alarm.add("DESCRIPTION", "discordbot description")
        alarm.add("TRIGGER", datetime.timedelta(seconds=0))
        todo.add_component(alarm)
        calendar.add_todo(todo.to_ical().decode("utf-8"))


def load_config() -> Config:
    with open(Path(__file__).parent.parent.joinpath("config.toml"), "rb") as fp:
        data = tomllib.load(fp)
    return Config(**data)


def parse_remindme_message(separators: list[str]) -> str:
    parser = argparse.ArgumentParser(description="CalDAV reinder CLI.")
    parser.add_argument(
        "remindme_string",
        nargs="+",
        type=str,
        help=textwrap.dedent(
            f"""
            A string in the format `<when> <separator> <what>` where <when> is a
            description *when* you want to be reminded, and <what> is *what*
            you want to be reminded of.
            <separator> can be one of the following: {separators}.

            If the string is just `reminders`, shows all current reminders.
        """
        ),
    )
    args = parser.parse_args()
    return " ".join(args.remindme_string)


def extract_todo(todo: CaldavTodo) -> ShortTodo:
    parsable_todo = IcalTodo.from_ical(todo.data)
    ical_todo = parsable_todo.walk("VTODO")[0]
    message = ical_todo.get("SUMMARY")
    start = ical_todo.get("DTSTART").dt
    return ShortTodo(message, start)


def show_reminders(config: Config):
    with DAVClient(
        url=config.caldav_base_url,
        username=config.caldav_username,
        password=config.caldav_password,
    ) as client:
        calendar = client.calendar(url=config.caldav_calendar_link)
        todos = calendar.todos()
        nice_todos = [extract_todo(todo) for todo in todos]
        for todo in sorted(nice_todos, key=lambda x: x.timestamp):
            print(f"{todo.timestamp.isoformat()}: {todo.description}")


def create_reminder(in_str: str, config: Config):
    time_str, reminder_content = parse_reminder(in_str, config.separators)
    parsing_settings = {
        "PREFER_DATES_FROM": "future",
        "RETURN_AS_TIMEZONE_AWARE": True,
        "TIMEZONE": config.timezone,
    }
    absolute_datetime = dateparser.parse(time_str, settings=parsing_settings)
    if absolute_datetime is None:
        raise ValueError(
            f"Sorry, i did not understand the time `{time_str}`\n"
            f"Remember not to use ambiguous times!"
        )
    add_caldav_todo(absolute_datetime, reminder_content, config)
    relative_datetime = absolute_datetime - datetime.datetime.now(
        tz=absolute_datetime.tzinfo
    )
    relative_time_str = build_timedelta_string(relative_datetime)
    print(
        f"Successfully added a caldav reminder for {absolute_datetime} (in {relative_time_str}) "
        f"with content:\n{reminder_content}"
    )


def build_timedelta_string(td: datetime.timedelta):
    years = td.days // 365
    months = (td.days % 365) // 30
    days = (td.days % 365) % 30
    hours = td.seconds // 3600
    minutes = (td.seconds % 3600) // 60
    seconds = (td.seconds % 3600) % 60
    ret = ""
    if years:
        ret += f"{years} years, "
    if months:
        ret += f"{months} months, "
    if days:
        ret += f"{days} days, "
    if hours:
        ret += f"{hours} hours, "
    if minutes:
        ret += f"{minutes} minutes, "
    if seconds:
        ret += f"{seconds} seconds, "
    if not ret:
        return ""
    return ret[:-2]


def main():
    config = load_config()
    in_str = parse_remindme_message(config.separators)
    if in_str == "reminders":
        show_reminders(config)
    else:
        create_reminder(in_str, config)


if __name__ == "__main__":
    main()
